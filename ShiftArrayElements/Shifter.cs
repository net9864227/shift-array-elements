﻿using System;

#pragma warning disable

namespace ShiftArrayElements
{
    public static class Shifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using <see cref="iterations"/> array for getting directions and iterations (see README.md for detailed instructions).
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="iterations">An array with iterations.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">iterations array is null.</exception>
        public static int[] Shift(int[] source, int[] iterations)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (iterations == null) throw new ArgumentNullException(nameof(iterations));
            if (iterations.Length == 0 || source.Length ==0) return source;
            int[] result = new int[source.Length];
            int move = 0;
            for (int i = 0; i < iterations.Length; i++)
            {
                if (i % 2 == 0) move -= iterations[i];
                else move += iterations[i];
            }

            move %= source.Length;

            for (int i = 0; i < source.Length; i++)
            {
                int pos;
                if (i + move < 0) pos = source.Length + (i + move);
                else if (i + move > source.Length - 1) pos = (i + move) - source.Length;
                else pos = i + move;
                result[pos] = source[i];
            }

            return result;
        }
    }
}
