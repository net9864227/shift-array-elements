﻿using System;

#pragma warning disable 

namespace ShiftArrayElements
{
    public static class EnumShifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using directions from <see cref="directions"/> array, one element shift per each direction array element.
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="directions">An array with directions.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">directions array is null.</exception>
        /// <exception cref="InvalidOperationException">direction array contains an element that is not <see cref="Direction.Left"/> or <see cref="Direction.Right"/>.</exception>
        public static int[] Shift(int[] source, Direction[] directions)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (directions == null) throw new ArgumentNullException(nameof(directions));
            if (directions.Length == 0) return source;
            int[] result = new int[source.Length];
            int move = 0;
            for(int i = 0; i < directions.Length; i++)
            {
                if (directions[i]!=Direction.Right && directions[i] != Direction.Left) throw new InvalidOperationException(nameof(directions));
                 else if (directions[i] == Direction.Left) move--;
                else if (directions[i] == Direction.Right) move++;
            }

            move %= source.Length;

            for (int i = 0; i < source.Length; i++)
            {
                int pos;
                if (i + move < 0) pos = source.Length + (i + move) ;
                else if (i+move >source.Length-1)  pos = (i + move) - source.Length;
                else pos = i + move;
                result[pos] = source[i];
            }

                return result;
        }
    }
}
